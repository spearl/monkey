# monkey

An interpreter for the Monkey programming language written in Go

![The Monkey Programming Language](https://cloud.githubusercontent.com/assets/1013641/22617482/9c60c27c-eb09-11e6-9dfa-b04c7fe498ea.png)

## What's monkey?

Monkey is a teaching programming language to learn about writing an interpreter originally from this book: [Writing An Interpreter In Go](https://interpreterbook.com/#the-monkey-programming-language).

Monkey has a C-like syntax, supports **variable bindings**, **prefix** and **infix operators**, has **first-class** and **higher-order functions**, can handle **closures** with ease and has **integers**, **booleans**, **arrays** and **hashes** built-in.

## Author

This is a implementation written from scratch following Writing an Interpreter in Go

## License

[GPL](LICENSE)
